// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "./KnowledgeToken.sol";

contract Engine is AccessControl {
    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");

    KnowledgeToken private _knowledgeToken;

    constructor(address knowledgeTokenAddress) {
        _knowledgeToken = KnowledgeToken(knowledgeTokenAddress);
    }

    function mint(address to, uint256 amount) public onlyRole(ADMIN_ROLE){
        _knowledgeToken.mint(to, amount);
    }

 function burn(address from, uint256 amount) external onlyRole(ADMIN_ROLE){
        _knowledgeToken.burn(from, amount);
    }

    function transfer(address from, address to, uint256 amount) external onlyRole(ADMIN_ROLE){
        _knowledgeToken.transferFrom(from, to, amount);
    }

    // function distributeKnowledge(address[] memory recipients, uint256 amount) external {
    //     require(hasRole(ADMIN_ROLE, msg.sender), "Caller is not a controller");
    //     for (uint256 i = 0; i < recipients.length; i++) {
    //         _knowledgeToken.mint(recipients[i], amount);
    //     }
    // }
    
    // Knowledge distribution function
    function distributeTokens(address[] memory recipients, uint256[] memory amounts) public onlyRole(ADMIN_ROLE){
        require(recipients.length == amounts.length, "Invalid input arrays");

        for (uint256 i = 0; i < recipients.length; i++) {
            _knowledgeToken.transfer(recipients[i], amounts[i]);
        }
    }

        function bulkTransfer(address[] memory recipients, uint256[] memory amounts) public onlyRole(ADMIN_ROLE){
        require(recipients.length == amounts.length, "Invalid input arrays");

        uint256 index = 0;
        _bulkTransfer(recipients, amounts, index);
    }

    function _bulkTransfer(address[] memory recipients, uint256[] memory amounts, uint256 index) private {
        if (index < recipients.length) {
            _knowledgeToken.transferFrom(msg.sender, recipients[index], amounts[index]);
            index++;
            if (index < recipients.length) {
                _bulkTransfer(recipients, amounts, index);
            }
        }
    }

}



// to do batch transactions
