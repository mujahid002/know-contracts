// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

contract KnowledgeToken is ERC20, AccessControl {
    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");

    constructor(address admin, uint256 initialSupply) ERC20("KnowledgeToken", "KNOW") {
        _grantRole(ADMIN_ROLE, admin);
        _mint(admin, initialSupply * (10 ** (decimals()+6)));
    }

    function mint(address admin, uint256 amount) public onlyRole(ADMIN_ROLE) {
        _mint(admin, amount);
    }

    function burn(address admin, uint256 amount) public onlyRole(ADMIN_ROLE) {
        _burn(admin, amount);
    }
}
