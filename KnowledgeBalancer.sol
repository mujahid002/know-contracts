// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "./KnowledgeToken.sol";

contract KnowledgeBalancer {
    KnowledgeToken private _knowledgeToken;

    constructor(address knowledgeTokenAddress) {
        _knowledgeToken = KnowledgeToken(knowledgeTokenAddress);
    }
}
